﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Cztery 
{
    class Column
    {
        Ellipse[] tabEllipse = new Ellipse[6];

        public Column(Grid g1)
        {
            for (int i = 0; i < tabEllipse.Length; i++)
            {
                tabEllipse[i] = new Ellipse()
                {
                    Height = 40,
                    Width = 40,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Bottom,
                    Margin = new Thickness(0,0,0,i*50+5),
                    StrokeThickness = 1
                };
                tabEllipse[i].Fill = new SolidColorBrush(Colors.White);
                g1.Children.Add(tabEllipse[i]);
            }
        }

        public void podswietlenie(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < tabEllipse.Length; i++)
            {
                tabEllipse[i].StrokeThickness = 5;
                tabEllipse[i].Stroke= new SolidColorBrush(Colors.Cyan);
            }
        }

        public void przywrocDomyslne(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < tabEllipse.Length; i++)
            {
                tabEllipse[i].StrokeThickness = 1;
                tabEllipse[i].Stroke = null;
            }
        }

        public void koloruj(object sender, MouseEventArgs e)
        {
           
            for (int i = 0; i < tabEllipse.Length ; i++)
            {
                if (tabEllipse[i].Fill.ToString() == "#FFFFFFFF")
                {
                    tabEllipse[i].Fill= new SolidColorBrush(StanGry.KolorGracza());
                    break;
                }
            }
        }

        public void kolorujKomputer()
        {
         //   MessageBox.Show("Pokolorowalo kompa");
            for (int i = 0; i < tabEllipse.Length; i++)
            {
                if (tabEllipse[i].Fill.ToString() == "#FFFFFFFF")
                {
                    tabEllipse[i].Fill = new SolidColorBrush(StanGry.KolorGracza());
                    break;
                }
            }
        }


       }

    }

