﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace Cztery
{
    /// <summary>
    /// Interaction logic for PoleGry.xaml
    /// </summary>
    public partial class PoleGry : Window
    {

        private Grid[] tablicaGrid;
        private Column[] tablicaKolumn;

        public PoleGry()
        {
            InitializeComponent();
            punkty.Text = "1 vs 2\n" + StanGry.punktyG1 + ":" + StanGry.punktyG2;
            tablicaGrid = new Grid[] { grid1, grid2, grid3, grid4, grid5, grid6, grid7 };
            tablicaKolumn = new Column[tablicaGrid.Length];

            for (var i = 0; i < tablicaGrid.Length; i++)
            {
                tablicaKolumn[i] = new Column(tablicaGrid[i]);
              
                    tablicaGrid[i].MouseEnter += tablicaKolumn[i].podswietlenie;
                    tablicaGrid[i].MouseLeave += tablicaKolumn[i].przywrocDomyslne;
                    tablicaGrid[i].MouseLeftButtonUp += tablicaKolumn[i].koloruj;
            }
          if (StanGry.graKomputer && StanGry.czyjaKolej == 2)
            {
                 tablicaKolumn[StanGry.RuchKomputera()].kolorujKomputer();
                StanGry.zmienGracza();
            }

           KtoryGracz.Text = "Kolej gracza: " + StanGry.czyjaKolej;
           KtoryGracz.Foreground = new SolidColorBrush(StanGry.KolorGracza());
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StanGry.punktyG1 = 0;
            StanGry.punktyG2 = 0;
            PoleGry nowepole =new PoleGry();
            nowepole.Show();
            StanGry.zeruj();
           this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void tablicaSiatek_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
           
            for (var i = 0; i < tablicaGrid.Length; i++)
            {
                if (tablicaGrid[i].IsMouseOver)
                {
                    for (var j = 0; j < 6; j++)
                    {
                        if (StanGry.Macierz[i, j] == 0)
                        {
                            if (!StanGry.graKomputer)
                                StanGry.Macierz[i, j] = StanGry.czyjaKolej;

                           if (StanGry.graKomputer && StanGry.czyjaKolej == 1)
                            {
                                StanGry.Macierz[i, j] = StanGry.czyjaKolej;
                                if (!StanGry.wygrana())
                                {
                                    StanGry.zmienGracza();
                                    tablicaKolumn[StanGry.RuchKomputera()].kolorujKomputer();
                                }
                            }
                            if (StanGry.wygrana() || StanGry.remis)
                            {
                                StanGry.aktualizujPunkty();
                                RevengeWindow rewanz =new RevengeWindow();
                                rewanz.ShowDialog();
                                  if(rewanz.IsEnabled)
                                    this.Close();
                            } else StanGry.zmienGracza();
                            
                            break;
                        }
                    }
                }
            }

            KtoryGracz.Text = "Kolej gracza: " + StanGry.czyjaKolej;
            KtoryGracz.Foreground = new SolidColorBrush(StanGry.KolorGracza());


        }




    }
}
