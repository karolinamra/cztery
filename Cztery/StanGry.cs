﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;


namespace Cztery
{
    public static class StanGry
    {
        public static int czyjaKolej = 1;
        public static  Color kolorG1= new Color();
        public static Color kolorG2 = new Color();
        public static int punktyG1 = 0;
        public static int punktyG2 = 0;
        public static bool remis = false;
        public static bool graKomputer=false;

        public static int [,] Macierz = new int[7,6];
        
        static StanGry()
        {
            Random rnd= new Random();
            czyjaKolej = rnd.Next(1, 3);
            kolorG1= Color.FromRgb(163,232,124);
            kolorG2 = Color.FromRgb(163,124,232);
        }

        public static void aktualizujPunkty()
        {
            if (remis)
            {
                punktyG1++;
                punktyG2++;
            }
            else
            if (czyjaKolej == 1)
                punktyG1++;
            else punktyG2++;
        }
        public static void zeruj()
        {
            for (int i = 0; i < 7; i++)
                for (int j = 0; j < 6; j++)
                    Macierz[i, j] = 0;
        }

        public static Color KolorGracza()
        {
            if (czyjaKolej == 1)
                return kolorG1;
            else
                return kolorG2;
        }

        public static void zmienGracza()
        {
            if (czyjaKolej == 1)
                czyjaKolej = 2;
            else
                czyjaKolej = 1;
        }

        public static bool wygrana()
        {
            int countpoziom = 1;
            int countpion = 1;
            int r = 0;
            //sprawdzenie czy remis
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (Macierz[j, i] == 0) r = 0;
                    else r++;
                    if (r == 42)
                        remis = true;
                    else remis = false;
                }
            }


            //wykrywanie wygranej w pionie
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    if ((Macierz[i, j] == Macierz[i, j + 1])&&(Macierz[i,j]!=0))
                        countpoziom++;
                    else countpoziom = 1;
                    if (countpoziom == 4) return true;
                }
                countpoziom = 1;
            }
            //wykrywanie wygranej w poziomie
            for (int j = 0; j < 6; j++)
            {
                for (int i = 0; i < 6; i++)
                {
                    if ((Macierz[i, j] == Macierz[i + 1, j]) && (Macierz[i, j] != 0))
                        countpion++;
                    else countpion = 1;
                    if (countpion == 4) return true;
                }
                countpion = 1;
            }
            
            //wykrywanie po skosach
            var k1 = 0;
            var k2 = 0;
            var k3 = 0;
            var k4 = 0;
            var count1 = 1;
            var count2 = 1;
            var count3 = 1;
            var count4 = 1;
            //skos1
            for (int j = 0; j < 3; j++)
            {
                k1 = j;
                for (int i = 0; k1 < 5; i++, k1++)
                {
                    if (Macierz[i, k1] != 0 && (Macierz[i, k1] == Macierz[i + 1, k1 + 1]))
                        count1++;
                    else count1 = 1;
                    if (count1 == 4) return true;
                }
                count1 = 1;
            }

            //skos2
            for(int j = 1; j < 4; j++)
            {
                k2 = j;
                for (int i = 0; k2 < 6; i++,k2++)
                {
                    if ((Macierz[k2, i] != 0) && (Macierz[k2, i] == Macierz[k2 + 1, i + 1]))
                        count2++;
                    else count2 = 1;
                    if (count2 == 4) return true;

                }
                count2 = 1;
            }
            //skos3
            for (int j = 5; j > 2; j--)
            {
                k3 = j;
                for (int i = 0; k3 > 0; i++, k3--)
                {
                    if (Macierz[i, k3] != 0 && (Macierz[i, k3] == Macierz[i + 1, k3 - 1]))
                        count3++;
                    else count3 = 1;
                    if (count3 == 4) return true;
                }
                count3 = 1;
            }

            //skos4
            for (int j = 1; j < 4; j++)
            {
                k4 = j;
                for (int i = 5; k4 < 6; i--,k4++)
                {
                    if (Macierz[k4, i] != 0 && (Macierz[k4, i] == Macierz[k4 + 1, i - 1]))
                        count4++;
                    else count4 = 1;
                    if (count4 == 4) return true;
                }
                count4 = 1;
            }
            return false;
        }

        public static int prawy(int[,] mac,int a,int b)
        {
            if (a >= 6)
                return -1;
            else return mac[a + 1, b];
        }

        public static int lewy(int[,] mac, int a, int b)
        {
            if (a <= 0)
                return -1;
            else return mac[a - 1, b];
        }

        public static int dolny(int[,] mac, int a, int b)
        {
            if (b <= 0)
                return -1;
            else return mac[a, b-1];
        }

        public static int prawyDolny(int[,] mac, int a, int b)
        {
            if (a >= 6 || b <= 0)
                return -1;
            else return mac[a + 1, b - 1];
        }

        public static int prawyGorny(int[,] mac, int a, int b)
        {
            if (a >= 6 || b >= 5)
                return -1;
            else return mac[a + 1, b + 1];
        }

        public static int lewyDolny(int[,] mac, int a, int b)
        {
            if (a <= 0 || b <= 0)
                return -1;
            else return mac[a - 1, b - 1];
        }
        public static int lewyGorny(int[,] mac, int a, int b)
        {
            if (a <= 0 || b >=5)
                return -1;
            else return mac[a - 1, b + 1];
        }


        public static int sprawdzenieWygranej(int[,] mac, int gracz)
        {
            for (var i = 0; i < 7; i++)
            {
                for (var j = 0; j < 6; j++)
                {

                    //sprawdzenie poziomu
                    if (Macierz[i, j] == 0 &&
                        ((prawy(Macierz, i, j) == gracz && prawy(Macierz, i + 1, j) == gracz &&
                          prawy(Macierz, i + 2, j) == gracz) ||
                         (lewy(Macierz, i, j) == gracz && prawy(Macierz, i, j) == gracz &&
                          prawy(Macierz, i + 1, j) == gracz) ||
                         (lewy(Macierz, i - 1, j) == gracz && lewy(Macierz, i, j) == gracz &&
                          prawy(Macierz, i, j) == gracz) ||
                         (lewy(Macierz, i - 2, j) == gracz && (lewy(Macierz, i - 1, j) == gracz) &&
                         lewy(Macierz, i, j) == gracz)) && (dolny(Macierz, i, j) != 0))
                    {
                        mac[i, j] = 2;
                        return i;
                    }

                    //sprawdzenie pionu
                    if (Macierz[i, j] == 0 && dolny(Macierz, i, j) == gracz && dolny(Macierz, i, j - 1) == gracz &&
                       dolny(Macierz, i, j - 2) == gracz)
                    {
                        mac[i, j] = 2;
                        return i;
                    }

                    //sprawdzenie skosów
                    if (Macierz[i, j] == 0 &&
                        ((lewyDolny(Macierz, i, j) == gracz && lewyDolny(Macierz, i - 1, j - 1) == gracz &&
                          lewyDolny(Macierz, i - 2, j - 2) == gracz) ||
                         (prawyGorny(Macierz, i, j) == gracz && lewyDolny(Macierz, i, j) == gracz &&
                          lewyDolny(Macierz, i - 1, j - 1) == gracz) ||
                         (prawyGorny(Macierz, i + 1, j + 1) == gracz && prawyGorny(Macierz, i, j) == gracz &&
                          lewyDolny(Macierz, i, j) == gracz) ||
                         (prawyGorny(Macierz, i, j) == gracz && prawyGorny(Macierz, i + 1, j + 1) == gracz &&
                          prawyGorny(Macierz, i + 2, j + 2) == gracz)) && dolny(Macierz, i, j) != 0)
                    {
                        mac[i, j] = 2;
                        return i;
                    }
                    if (Macierz[i, j] == 0 &&
                        ((prawyDolny(Macierz, i, j) == gracz && prawyDolny(Macierz, i + 1, j - 1) == gracz &&
                          prawyDolny(Macierz, i + 2, j - 2) == gracz) ||
                         (lewyGorny(Macierz, i, j) == gracz && prawyDolny(Macierz, i, j) == gracz &&
                          prawyDolny(Macierz, i + 1, j - 1) == gracz) ||
                         (lewyGorny(Macierz, i - 1, j + 1) == gracz && lewyGorny(Macierz, i, j) == gracz &&
                          prawyDolny(Macierz, i, j) == gracz) ||
                         (lewyGorny(Macierz, i, j) == gracz && lewyGorny(Macierz, i - 1, j + 1) == gracz &&
                          lewyGorny(Macierz, i - 2, j + 2) == gracz)) && dolny(Macierz, i, j) != 0)
                    {
                        mac[i, j] = 2;
                        return i; 
                    }
                }
            }
            return -1;
        }

        public static int RuchKomputera()
        {
            int komputer = 2;
            int czlowiek = 1;
            int rzad;
            int[,] macPom= new int[7,6];

            //sprawdzenie czy wygra komputer
            rzad = sprawdzenieWygranej(Macierz,2);
            if (rzad >= 0) return rzad;


            //obrona 
            rzad = sprawdzenieWygranej(Macierz,1);
            if (rzad >= 0)
                return rzad;
            
            //sprawdzenie czy po ruchu komputera nie bedzie pozniej wygranej gracza

 
            Random rnd1= new Random();
            var i = rnd1.Next(7);
           // for (var i = 0; i < 7; i++)
           // {
               for (var j = 0; j < 6; j++)
                {
                      if (Macierz[i,j]==0 )
                    {
                          //sprawdzenie czy jak komputer wykona ruch nie przyczyni sie do wygranej gracza 1
                        macPom = Macierz;
                        macPom[i, j] = 2;
                        if (sprawdzenieWygranej(macPom, 1) == -1)
                        {
                            Macierz[i, j] = 2;
                            return i;
                        }
                  }

            //    }
            }

            for (int j = 0; j < 7; j++)
            {
                for (int k = 0; k < 6; k++)
                {
                    if (Macierz[j, k] == 0)
                    {
                        Macierz[j, k] = 2;
                        return j;
                    }
                }
            }
            return -1;

            //zmienGracza();
        }

    }


}
