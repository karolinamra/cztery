﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Cztery
{
    /// <summary>
    /// Interaction logic for RevengeWindow.xaml
    /// </summary>
    public partial class RevengeWindow : Window
    {
        public RevengeWindow()
        {
            InitializeComponent();
            if (StanGry.remis)
                wygranaNapis.Text = "Remis";
            else
            {
                wygranaNapis.Text = "Wygrana gracza " + StanGry.czyjaKolej;
                wygranaNapis.Foreground = new SolidColorBrush(StanGry.KolorGracza());
            }
        }

   

        private void buttonNowaGra_Click(object sender, RoutedEventArgs e)
        {
            StanGry.zeruj();
            StanGry.punktyG1 = 0;
            StanGry.punktyG2 = 0;
            PoleGry nowePoleGry = new PoleGry();

            nowePoleGry.Show();
            this.Close();
           }
       

        private void buttonMenu_Click(object sender, RoutedEventArgs e)
        {
            StanGry.zeruj();
            StanGry.punktyG1 = 0;
            StanGry.punktyG2 = 0;
            MainWindow oknoWyboru = new MainWindow();
            oknoWyboru.Show();
            this.Close();
        }

        private void buttonRewanz_Click(object sender, RoutedEventArgs e)
        {
            StanGry.zeruj();
            PoleGry nowePoleGry = new PoleGry();
            nowePoleGry.Show();
            this.Close();  
        }

    }
}
