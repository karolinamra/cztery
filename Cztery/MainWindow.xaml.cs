﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cztery
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

           InitializeComponent();

        }

        private void koniec_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void graCzlowiek_Click(object sender, RoutedEventArgs e)
        {
            StanGry.graKomputer = false;
            PoleGry nowaGra =new PoleGry();
            nowaGra.Show();
            this.Close();
        }

        private void graKomputer_Click(object sender, RoutedEventArgs e)
        {
            StanGry.graKomputer = true;
            PoleGry nowaGra2 = new PoleGry();
            nowaGra2.Show();
            this.Close();
        }
    }
}
